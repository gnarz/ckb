/* ckb_keyname.c
 *
 * a helper lib for ckb_keyboard: find key names for keys returned by ckb_readkey()
 *
 * Gunnar Zötl <gz@tset.de> 2016
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdio.h>
#include "ckb_keyboard.h"
#include "ckb_keyname.h"
#include "mini_utf8.h"

#define KEYNAME_BUFSIZE 16

const char* ckb_keyname(ckb_key *key)
{
	static char buf[KEYNAME_BUFSIZE];

	if (key->label == CKB_ESCAPE) {
		return "[ESCAPE]";
	} else if (key->label == CKB_SPACE) {
		return "[SPACE]";
	} else if (key->label == CKB_TAB) {
		return "[TAB]";
	} else if (key->label == CKB_DEAD) {
		snprintf(buf, KEYNAME_BUFSIZE, "[DEAD%c]", key->sym);
		return buf;
	} else if (key->label < CKB_SPECIAL) {
		int l = mini_utf8_encode(key->sym, buf, KEYNAME_BUFSIZE);
		buf[l] = '\0';
		return buf;
	} else if (key->label == CKB_NONE) {
		return "[-none-]"; /* this should not happen */
	} else if (key->label > CKB_CONS) {
		snprintf(buf, KEYNAME_BUFSIZE, "[CONS%d]", key->label & 0xff);
		return buf;
	} else {
		switch (key->label) {
			case CKB_F1: return "[F1]";
			case CKB_F2: return "[F2]";
			case CKB_F3: return "[F3]";
			case CKB_F4: return "[F4]";
			case CKB_F5: return "[F5]";
			case CKB_F6: return "[F6]";
			case CKB_F7: return "[F7]";
			case CKB_F8: return "[F8]";
			case CKB_F9: return "[F9]";
			case CKB_F10: return "[F10]";
			case CKB_F11: return "[F11]";
			case CKB_F12: return "[F12]";
			case CKB_PRTSC: return "[PRTSC]";
			case CKB_SCROLL: return "[SCROLL]";
			case CKB_PAUSE: return "[PAUSE]";
			case CKB_BACKSPACE: return "[BACKSPACE]";
			case CKB_INSERT: return "[INSERT]";
			case CKB_HOME: return "[HOME]";
			case CKB_PGUP: return "[PGUP]";
			case CKB_TAB: return "[TAB]";
			case CKB_ENTER: return "[ENTER]";
			case CKB_DELETE: return "[DELETE]";
			case CKB_END: return "[END]";
			case CKB_PGDOWN: return "[PGDOWN]";
			case CKB_CAPS: return "[CAPS]";
			case CKB_LSHIFT: return "[LSHIFT]";
			case CKB_RSHIFT: return "[RSHIFT]";
			case CKB_LCTRL: return "[LCTRL]";
			case CKB_LMETA: return "[LMETA]";
			case CKB_ALT: return "[ALT]";
			case CKB_ALTGR: return "[ALTGR]";
			case CKB_RMETA: return "[RMETA]";
			case CKB_MENU: return "[MENU]";
			case CKB_SYSREQ: return "[SYSREQ]";
			case CKB_RCTRL: return "[RCTRL]";
			case CKB_UP: return "[UP]";
			case CKB_LEFT: return "[LEFT]";
			case CKB_DOWN: return "[DOWN]";
			case CKB_RIGHT: return "[RIGHT]";
			case CKB_NUM: return "[NUM]";
			case CKB_NDIV: return "[NDIV]";
			case CKB_NMUL: return "[NMUL]";
			case CKB_NMINUS: return "[NMINUS]";
			case CKB_NPLUS: return "[NPLUS]";
			case CKB_NENTER: return "[NENTER]";
			case CKB_N0: return "[N0]";
			/*case CKB_NCOMMA: return "[NCOMMA]"; same as above */
			case CKB_NDOT: return "[NDOT]";
			case CKB_N1: return "[N1]";
			case CKB_N2: return "[N2]";
			case CKB_N3: return "[N3]";
			case CKB_N4: return "[N4]";
			case CKB_N5: return "[N5]";
			case CKB_N6: return "[N6]";
			case CKB_N7: return "[N7]";
			case CKB_N8: return "[N8]";
			case CKB_N9: return "[N9]";
			default:
				snprintf(buf, KEYNAME_BUFSIZE, "[-unk-%04x-]", key->label);
				return buf;
		}
	}
}