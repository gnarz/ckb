/* ckb_keyboard.h
 *
 * a simple library to read events from a medium raw linux console keyboard
 *
 * Gunnar Zötl <gz@tset.de> 2016
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#ifndef ckb_keyboard_h
#define ckb_keyboard_h

#include <stdint.h>

/* if you change these, you must recompile ckb_keyboard.c!
 */
#define DEFAULT_DEVICE "/dev/tty"
#define DEFAULT_RBSIZE 16

#define CKB_NONE 0xffff
#define CKB_ESCAPE 0x1b

/* this value marks the beginning of the special key label values.
 * Everything below this is considered a unicode value
 */
#define CKB_SPECIAL 0x8000
/* marker for dead chars. Char code is in key->sym. */
#define CKB_DEAD 0x8000
 /* function keys */
#define CKB_F1 0x8001
#define CKB_F2 0x8002
#define CKB_F3 0x8003
#define CKB_F4 0x8004
#define CKB_F5 0x8005
#define CKB_F6 0x8006
#define CKB_F7 0x8007
#define CKB_F8 0x8008
#define CKB_F9 0x8009
#define CKB_F10 0x800a
#define CKB_F11 0x800b
#define CKB_F12 0x800c
/* misc keys */
#define CKB_PRTSC 0x800d
#define CKB_SCROLL 0x800e
#define CKB_PAUSE 0x800f
#define CKB_BACKSPACE 0x8010
#define CKB_INSERT 0x8011
#define CKB_HOME 0x8012
#define CKB_PGUP 0x8013
#define CKB_TAB 0x09
#define CKB_ENTER 0x8014
#define CKB_DELETE 0x8015
#define CKB_END 0x8016
#define CKB_PGDOWN 0x8017
#define CKB_MENU 0x8022
#define CKB_SYSREQ 0x8023
#define CKB_CAPS 0x8018
/* modifier keys */
#define CKB_LSHIFT 0x8019
#define CKB_RSHIFT 0x801a
#define CKB_LCTRL 0x801b
#define CKB_LMETA 0x801c
#define CKB_ALT 0x801e
#define CKB_SPACE 0x20
#define CKB_ALTGR 0x801f
#define CKB_RMETA 0x8020
#define CKB_RCTRL 0x8021
/* cursor keys */
#define CKB_UP 0x8024
#define CKB_LEFT 0x8025
#define CKB_DOWN 0x8026
#define CKB_RIGHT 0x8027
/* num block */
#define CKB_NUM 0x8028
#define CKB_NDIV 0x8029
#define CKB_NMUL 0x802a
#define CKB_NMINUS 0x802b
#define CKB_NPLUS 0x802c
#define CKB_NENTER 0x802d
#define CKB_N0 0x802e
#define CKB_NCOMMA 0x802f
#define CKB_NDOT 0x802f
#define CKB_N1 0x8030
#define CKB_N2 0x8031
#define CKB_N3 0x8032
#define CKB_N4 0x8033
#define CKB_N5 0x8034
#define CKB_N6 0x8035
#define CKB_N7 0x8036
#define CKB_N8 0x8037
#define CKB_N9 0x8038
/* virtual console switching codes. First console is 0xF001 etc */
#define CKB_CONS 0xF000

/* key modifiers. There are bits to be combined with or (|)
 */
#define CKB_MOD_SHIFT 1
#define CKB_MOD_CTRL 2
#define CKB_MOD_ALT 4
#define CKB_MOD_ALTGR 8
#define CKB_MOD_META 16
#define CKB_MOD_CAPS 32
#define CKB_MOD_NUM 64
#define CKB_MOD_SCROLL 128

/* key events
 */
#define CKB_E_NONE 0
#define CKB_E_PRESS 1
#define CKB_E_RELEASE 2
#define CKB_E_REPEAT 3

/* the structure for one key event.
 */
typedef struct {
	uint8_t which;	/* one of the CKB_E_* constants. */
	uint8_t code;	/* raw keyboard key code */
	uint8_t mod;	/* modifier combination, any combination of the CKB_MOD_*
					   bits */
	uint16_t label;	/* some sort of name or label for the key, either the
					   lower case char or obe of the CKB_* key values above. */
	uint16_t sym;	/* the actual unicode value associated with this key and
					   the active modifier keys plus accent from dead keys */
	uint16_t raw;	/* raw console key symbol as returned by ioctl(KDGKBENT)
					   with all modifiers, for debugging use */
} ckb_key;

/* the opaque structure representing a keyboard
 */
typedef struct _ckb_kbd ckb_keyboard;

/* ckb_init
 * open keyboard device, initialize it and return a pointer to a keyboard
 * structure. This structure must be passed to all other ckb_* functions,
 * but is otherwise opaque.
 *
 * Arguments:
 *	altdev		the name of a device to use, or 0 to use the default /dev/tty
 *	altrbsize	the desired size of the ringbuffer. Must be at least 1, or
 *				specify 0 to use the default (DEFAULT_RBSIZE).
 *
 * Returns:
 *	1 on success, 0 on failure.
 */
ckb_keyboard* ckb_init(const char *altdev, int altrbsize);

/* ckb_close
 * close a keyboard device and deallocate the keyboard structure. You must
 * call this before exiting your application, otherwise the keyboard will
 * be completely unusable from the shell!
 *
 * Arguments:
 *	kbd			the keyboard handler structure to close and deallocate
 */
void ckb_close(ckb_keyboard *kbd);

/* ckb_poll
 * poll keyboard device for new keypresses.
 *
 * Arguments:
 *	kbd			keyboard device to poll.
 *
 * Returns:
 *	0 if an invalid or unknown key was read, or an error occurred, 1 if
 *	everything was ok.
 */
int ckb_poll(ckb_keyboard *kbd);

/* ckb_getkey
 * reads one key from the keyboard ring buffer. See the description of the
 * ckb_key fields above. The sym field is always the combined value of all
 * currently active modifier keys, plus the previously types accent (dead
 * char). There may be occurrences when a combination of an accent and a
 * subsequently typed char does not yield a value. In this case, key->label
 * will still hold the unmodified char, and key->sym will be KBD_NONE.
 *
 * Arguments:
 *	kbd			keyboard device to read
 *	key			pointer to a ckb_key structure to hold the result, if any.
 *
 * Returns:
 *	1 if a key was read, 0 if not. If 1 is returned, the key's data is copied
 *	into key.
 */
int ckb_getkey(ckb_keyboard *kbd, ckb_key *key);

/* ckb_ischar
 * checks whether a ckb_key value holds a valid char
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a char, 0 if not
 */
int ckb_ischar(ckb_key *key);

/* ckb_isfnkey
 * checks whether a ckb_key value holds a function key
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a function key, 0 if not
 */
int ckb_isfnkey(ckb_key *key);

/* ckb_isnumpad
 * checks whether a ckb_key value holds a key from the numpad. Note that
 * if num lock is not active, the keys 0-9 and ,/. from the num pad will
 * be returned as if they were insert, delete, ... keys, so this will 
 * return them as not being numpad keys.
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a numpad key, 0 if not
 */
int ckb_isnumpad(ckb_key *key);

/* ckb_isfnkey
 * checks whether a ckb_key value holds a modifier key (shift, control, alt,
 * meta)
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a moddifier key, 0 if not
 */
int ckb_ismod(ckb_key *key);

/* ckb_isfnkey
 * checks whether a ckb_key value holds a lock key (caps, num or scroll lock)
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a lock key, 0 if not
 */
int ckb_islock(ckb_key *key);

/* ckb_iskeyup
 * checks whether a ckb_key value holds a key release event
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is a key release event, 0 if not
 */
#define ckb_iskeyup(key) ((key)->which == CKB_E_RELEASE)

/* ckb_iskeydown
 * checks whether a ckb_key value holds a key press or a key repeat event
 *
 * Arguments:
 *	key			ckb_key struct to check
 *
 * Returns:
 *	1 if it is one of the checked events, 0 if not
 */
#define ckb_iskeydown(key) (!ckb_iskeyup(key))

/* ckb_resizebuf
 * resize the keyboard ring buffer. Default size is DEFAULT_RBSIZE, any
 * size > 0 is valid. This flushes any data still in the buffer!
 *
 * Arguments:
 *	kbd			keyboard device to resize ringbuffer for.
 *
 * Returns:
 *	1 on success, 0 on failure.
 */
int ckb_resizebuf(ckb_keyboard *kbd, int size);

/* ckb_flushbuf
 * flushes (empties) the keyboard ringbuffer. You probably don't want to do
 * this, unless you really don't care about any data left in the buffer.
 *
 * Arguments:
 *	kbd			keyboard device to flush ringbuffer for.
 */
void ckb_flushbuf(ckb_keyboard *kbd);

#endif /* ckb_keyboard_h */
