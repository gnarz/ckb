/* ckb_keyname.h
 *
 * a helper lib for ckb_keyboard: find key names for keys returned by ckb_readkey()
 * This helper lib is optional, you don't need it to use ckb_keyboard.
 *
 * Gunnar Zötl <gz@tset.de> 2016
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#ifndef ckb_keyname_h
#define ckb_keyname_h

/* ckb_keyname
 * returns a name for a key. In the case of "standard" keys, this will be the
 * unicode value of the char returned by the keypress, otherwise it will be a
 * name in [brackets] indicating what key it is.
 * In the case of unknown keys, the returned name will be [-unk-XXXX-],  where
 * XXXX is a 4 digit hex number indicating the kernel's idea of the key name
 * (i.e. what was returned by ioctl(KDGKBENT) with only the key's code).
 * The returned string is static and may be modified upon the next call to
 * this function.
 *
 * Arguments:
 *	key			key to find a name for
 *
 * Returns:
 *	some name for the key.
 */
const char* ckb_keyname(ckb_key *key);

#endif /* ckb_keyname_h */
