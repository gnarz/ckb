/* ckb_keyboard.c
 *
 * a simple library to read events from a medium raw linux console keyboard
 *
 * Gunnar Zötl <gz@tset.de> 2016
 * Released under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <linux/keyboard.h>
#include <linux/kd.h>

#include "ckb_keyboard.h"

#define K_LMETA 125
#define K_RMETA 126
#define K_MENU 127

struct _ckb_kbd {
	/* saved previous state of tty */
	int old_mode;
	struct termios old_termios;
	int old_kbled;

	/* working data for keyboard */
	int fd;
	struct kbdiacrsuc diac_table;

	/* current state */
	uint16_t accent;
	uint8_t modifiers;
	uint8_t pressed[128];
	
	/* ringbuffer */
	uint16_t rbsize;
	int16_t rbfirst, rblast;
	ckb_key *ringbuffer;
};

/*** keyboard ringbuffer handling ***/
static int ckb_insert_keypress(ckb_keyboard *kbd, ckb_key *key)
{
	int nxtlast = (kbd->rblast + 1) % kbd->rbsize;
	if (nxtlast == kbd->rbfirst) {
		return 0;
	}
	kbd->ringbuffer[kbd->rblast] = *key;
	kbd->rblast = nxtlast;
	return 1;
}

int ckb_resizebuf(ckb_keyboard *kbd, int size)
{
	if (size < 1) { return 0; }
	ckb_key *newbuf = calloc(size, sizeof(ckb_key));
	if (newbuf) {
		free(kbd->ringbuffer);
		kbd->ringbuffer = newbuf;
		kbd->rbsize = size;
		kbd->rbfirst = 0;
		kbd->rblast = 0;
		return 1;
	}
	return 0;
}

void ckb_flushbuf(ckb_keyboard *kbd)
{
	kbd->rbfirst = 0;
	kbd->rblast = 0;
}

static int ckb_trans_accent(ckb_keyboard *kbd, uint16_t sym)
{
	if (sym == ' ') {
		return kbd->accent;
	}
	unsigned int i;
	for (i = 0; i < kbd->diac_table.kb_cnt; ++i) {
		const struct kbdiacruc *entry = &kbd->diac_table.kbdiacruc[i];
		if (entry->diacr == kbd->accent && entry->base == sym) {
			return entry->result;
		}
	}
	return sym;
}

static int ckb_trans_key(ckb_keyboard *kbd, ckb_key *key)
{
	/* diacritics handling */
	if (KTYP(key->sym) == KT_DEAD) {
		switch (key->sym) {
			case K_DGRAVE: key->sym = 96; break;
			case K_DACUTE: key->sym = 39; break;
			case K_DCIRCM: key->sym = 94; break;
			case K_DTILDE: key->sym = 126; break;
			case K_DDIERE: key->sym = 34; break;
			case K_DCEDIL: key->sym = 44; break;
			default: return 0;
		}
		kbd->accent = key->sym;
		key->label = CKB_DEAD;
		return 1;
	}

	/* Backspace vs. Delete */
	if (key->code == 14) {
		key->label = CKB_BACKSPACE;
		key->sym = 0x08;
		kbd->accent = 0;
		return 1;
	}
	if (key->code == 111) {
		key->label = CKB_DELETE;
		key->sym = 0x7f;
		kbd->accent = 0;
		return 1;
	}

	/* "regular" keys */
	if (KTYP(key->sym) == KT_LATIN || KTYP(key->sym) == KT_LETTER || KTYP(key->sym) == KT_ASCII) {
		key->label = KVAL(key->label);
		key->sym = KVAL(key->sym);
		if (kbd->accent) {
			key->sym = ckb_trans_accent(kbd, key->sym);
			kbd->accent = 0;
		}
		return 1;
	}

	/* console switches */
	if (KTYP(key->sym) == KT_CONS) {
		key->label = CKB_CONS + KVAL(key->sym) + 1;
		key->sym = CKB_NONE;
		kbd->accent = 0;
		return 1;
	}

	int isnumlk = ((kbd->modifiers & CKB_MOD_NUM) != 0);
	key->sym = CKB_NONE;

	/* unknown keys */
	if (key->code == K_LMETA) {
		key->label = CKB_LMETA;
		return 1;
	} else if (key->code == K_RMETA) {
		key->label = CKB_RMETA;
		return 1;
	} else if (key->code == K_MENU) {
		if (kbd->modifiers & CKB_MOD_META) {
			key->label = CKB_SYSREQ;
		} else {
			key->label = CKB_MENU;
		}
		kbd->accent = 0;
		return 1;
	}

	switch (key->label) {
		case K_F1: key->label = CKB_F1; break;
		case K_F2: key->label = CKB_F2; break;
		case K_F3: key->label = CKB_F3; break;
		case K_F4: key->label = CKB_F4; break;
		case K_F5: key->label = CKB_F5; break;
		case K_F6: key->label = CKB_F6; break;
		case K_F7: key->label = CKB_F7; break;
		case K_F8: key->label = CKB_F8; break;
		case K_F9: key->label = CKB_F9; break;
		case K_F10: key->label = CKB_F10; break;
		case K_F11: key->label = CKB_F11; break;
		case K_F12: key->label = CKB_F12; break;
		case K_HOLE: key->label = CKB_PRTSC; break;
		case K_HOLD: key->label = CKB_SCROLL; break;
		case K_PAUSE: key->label = CKB_PAUSE; break;
		case K_INSERT: key->label = CKB_INSERT; break;
		case K_FIND: key->label = CKB_HOME; break;
		case K_PGUP: key->label = CKB_PGUP; break;
		/*case 0x09: key->label = CKB_TAB; break;*/
		case K_ENTER:
			key->label = CKB_ENTER;
			key->sym = '\n';
			break;
		case K_SELECT: key->label = CKB_END; break;
		case K_PGDN: key->label = CKB_PGDOWN; break;
		case K(KT_LOCK,6): key->label = CKB_CAPS; break;
		case K_SHIFTL: key->label = CKB_LSHIFT; break;
		case K_SHIFTR: key->label = CKB_RSHIFT; break;
		case K_SHIFT: key->label = key->code == 42 ? CKB_LSHIFT : CKB_RSHIFT; break;
		case K_CTRLL: key->label = CKB_LCTRL; break;
		case K_CTRLR: key->label = CKB_RCTRL; break;
		case K_CTRL: key->label = key->code == 29 ? CKB_LCTRL : CKB_RCTRL; break;
		case K_ALT: key->label = CKB_ALT; break;
		case K_ALTGR: key->label = CKB_ALTGR; break;
		/*case 0x20: key->label = CKB_SPACE; break;*/
		case K_UP: key->label = CKB_UP; break;
		case K_LEFT: key->label = CKB_LEFT; break;
		case K_DOWN: key->label = CKB_DOWN; break;
		case K_RIGHT: key->label = CKB_RIGHT; break;
		case K_NUM: key->label = CKB_NUM; break;
		/* num block */
		case K_PSLASH:
			key->label = CKB_NDIV;
			key->sym = '/';
			break;
		case K_PSTAR:
			key->label = CKB_NMUL;
			key->sym = '*';
			break;
		case K_PMINUS:
			key->label = CKB_NMINUS;
			key->sym = '-';
			break;
		case K_PPLUS:
			key->label = CKB_NPLUS;
			key->sym = '+';
			break;
		case K_PENTER:
			key->label = CKB_NENTER;
			key->sym = '\n';
			break;
		case K_P0:
			if (isnumlk) {
				key->label = CKB_N0;
				key->sym = '0';
			} else {
				key->label = CKB_INSERT;
			}
			break;
		case K_PCOMMA:
			if (isnumlk) {
				key->label = CKB_NCOMMA;
				key->sym = ',';
			} else {
				key->label = CKB_DELETE;
			}
			break;
		case K_PDOT:
			if (isnumlk) {
				key->label = CKB_NDOT;
				key->sym = '.';
			} else {
				key->label = CKB_DELETE;
			}
			break;
		case K_P1:
			if (isnumlk) {
				key->label = CKB_N1;
				key->sym = '1';
			} else {
				key->label = CKB_END;
			}
			break;
		case K_P2:
			if (isnumlk) {
				key->label = CKB_N2;
				key->sym = '2';
			} else {
				key->label = CKB_DOWN;
			}
			break;
		case K_P3:
			if (isnumlk) {
				key->label = CKB_N3;
				key->sym = '3';
			} else {
				key->label = CKB_PGDOWN;
			}
			break;
		case K_P4:
			if (isnumlk) {
				key->label = CKB_N4;
				key->sym = '4';
			} else {
				key->label = CKB_LEFT;
			}
			break;
		case K_P5:
			if (isnumlk) {
				key->label = CKB_N5;
				key->sym = '5';
			} else {
				key->label = CKB_NONE;	/* should I do something with this? */
			}
			break;
		case K_P6:
			if (isnumlk) {
				key->label = CKB_N6;
				key->sym = '6';
			} else {
				key->label = CKB_RIGHT;
			}
			break;
		case K_P7:
			if (isnumlk) {
				key->label = CKB_N7;
				key->sym = '7';
			} else {
				key->label = CKB_HOME;
			}
			break;
		case K_P8:
			if (isnumlk) {
				key->label = CKB_N8;
				key->sym = '8';
			} else {
				key->label = CKB_UP;
			}
			break;
		case K_P9:
			if (isnumlk) {
				key->label = CKB_N9;
				key->sym = '9';
			} else {
				key->label = CKB_PGUP;
			}
			break;
		default: /* unknown / invalid key */
			key->label = CKB_NONE;
			kbd->accent = 0;
			return 0;
	}

	if (!ckb_ismod(key) && !ckb_islock(key)) {
		kbd->accent = 0;
	}

	return 1;
}

static void ckb_update_leds(ckb_keyboard *kbd)
{
 	int ledval = kbd->modifiers & CKB_MOD_CAPS ? LED_CAP : 0;
	ledval |= kbd->modifiers & CKB_MOD_NUM ? LED_NUM : 0;
	ledval |= kbd->modifiers & CKB_MOD_SCROLL ? LED_SCR : 0;
	ioctl(kbd->fd, KDSKBLED, ledval);
}

static void ckb_update_modifiers(ckb_keyboard *kbd, int down, int val)
{
	switch (val) {
		case K_SHIFT:
		case K_SHIFTL:
		case K_SHIFTR:
			if (down) {
				kbd->modifiers |= CKB_MOD_SHIFT;
			} else {
				kbd->modifiers &= ~CKB_MOD_SHIFT;
			}
			break;
		case K_CTRL:
		case K_CTRLL:
		case K_CTRLR:
			if (down) {
				kbd->modifiers |= CKB_MOD_CTRL;
			} else {
				kbd->modifiers &= ~CKB_MOD_CTRL;
			}
			break;
		case K_LMETA:
		case K_RMETA:
			if (down) {
				kbd->modifiers |= CKB_MOD_META;
			} else {
				kbd->modifiers &= ~CKB_MOD_META;
			}
			break;
		case K_ALT:
			if (down) {
				kbd->modifiers |= CKB_MOD_ALT;
			} else {
				kbd->modifiers &= ~CKB_MOD_ALT;
			}
			break;
		case K_ALTGR:
			if (down) {
				kbd->modifiers |= CKB_MOD_ALTGR;
			} else {
				kbd->modifiers &= ~CKB_MOD_ALTGR;
			}
			break;
		case K_CAPS:
		case K_CAPSON:
		case K_CAPSSHIFT:
		case K(KT_LOCK,6):	/* code on my system, eq to K_CTRLLOCK... weird? */
			if (down) {
				kbd->modifiers ^= CKB_MOD_CAPS;
				ckb_update_leds(kbd);
			}
			break;
		case K_NUM:
			if (down) {
				kbd->modifiers ^= CKB_MOD_NUM;
				ckb_update_leds(kbd);
			}
			break;
		case K_HOLD:
			if (down) {
				kbd->modifiers ^= CKB_MOD_SCROLL;
				ckb_update_leds(kbd);
			}
			break;
		default:;
	}
}

static int ckb_handle_event(ckb_keyboard *kbd, unsigned char code)
{
	struct kbentry entry;
	int symval, labelval;
	int status;

	if (code & 0x80) {
		status = CKB_E_RELEASE;
		kbd->pressed[code & 0x7f] = 0;
	} else if (kbd->pressed[code]) {
		status = CKB_E_REPEAT;
	} else {
		status = CKB_E_PRESS;
		kbd->pressed[code] = 1;
	}
	code = code & 0x7f;

	if (ioctl(kbd->fd, KDSKBMODE, K_UNICODE) < 0) {
		perror("ckb_handle_event: ioctl(KDSKBMODE) failed.\n");
		return 0;
	}
	/* Look up the keysym without modifiers, which will give us a key label
	 */
	entry.kb_table = 0;
	entry.kb_index = code;
	if (ioctl(kbd->fd, KDGKBENT, &entry) < 0) {
		perror("ckb_handle_event: ioctl(KDGKBENT) failed.\n");
		return 0;
	}
	labelval = entry.kb_value & 0x0fff;
	if (labelval == K_ALT && (code == K_LMETA || code == K_RMETA)) {
		labelval = code;
	}

	ckb_update_modifiers(kbd, status != CKB_E_RELEASE, labelval);

	/* Now look up the full keysym in the kernel's table
	 */

	/* calculate kernel-like shift value */
	entry.kb_table  = (kbd->modifiers & CKB_MOD_SHIFT) ? (1 << KG_SHIFT) : 0;
	entry.kb_table |= (kbd->modifiers & CKB_MOD_CTRL) ? (1 << KG_CTRL) : 0;
	entry.kb_table |= (kbd->modifiers & CKB_MOD_ALT) ? (1 << KG_ALT) : 0;
	entry.kb_table |= (kbd->modifiers & CKB_MOD_ALTGR) ? (1 << KG_ALTGR) : 0;
	entry.kb_table |= (kbd->modifiers & CKB_MOD_META) ? (1 << KG_ALT) : 0;
	/* this seems not to work...
	   entry.kb_table |= (kbd->modifiers & CKB_MOD_CAPS) ? (1 << KG_CAPSSHIFT) : 0;
	 * but this does:
	 */
	if (KTYP(labelval) == KT_LETTER) {
		entry.kb_table |= (kbd->modifiers & CKB_MOD_CAPS) ? (1 << KG_SHIFT) : 0;
	}

	entry.kb_index = code;
	if (ioctl(kbd->fd, KDGKBENT, &entry) < 0) {
		perror("ckb_handle_event: ioctl(KDGKBENT) failed.\n");
		return 0;
	}
	symval = entry.kb_value & 0x0fff;

	/* Switch back to MEDIUMRAW */
	if (ioctl(kbd->fd, KDSKBMODE, K_MEDIUMRAW) < 0) {
		perror("ckb_handle_event: ioctl(KDSKBMODE) failed.\n");
		return 0;
	}

	ckb_key key;
	key.which = status;
	key.code = code;
	key.mod = kbd->modifiers;
	key.label = labelval;
	key.sym = symval;
	key.raw = symval;
	if (ckb_trans_key(kbd, &key)) {
		ckb_insert_keypress(kbd, &key);
	}

	return 1;
}

void ckb_close(ckb_keyboard *kbd)
{
	if (kbd->old_kbled != 0x7f) {
		ioctl(kbd->fd, KDSKBLED, kbd->old_kbled);
	}

	if (ioctl(kbd->fd, KDSKBMODE, kbd->old_mode) < 0) {
		perror("ckb_close: restore mode failed");
	}

	if (tcsetattr(kbd->fd, TCSANOW, &kbd->old_termios) < 0) {
		perror("ckb_close: tcsetattr failed");
	}

	close(kbd->fd);

	if (kbd->ringbuffer) {
		free(kbd->ringbuffer);
	}

	free(kbd);
}

static int is_in_background = 0;
void sighandler(int num)
{
	if (num == SIGTTIN || num == SIGTTOU) {
		is_in_background = 1;
	}
}

ckb_keyboard* ckb_init(const char *altdev, int altrbsize)
{
	const char *filename = DEFAULT_DEVICE;
	if (altdev) { filename = altdev; }
	int rbsize = DEFAULT_RBSIZE;
	if (altrbsize > 0) { rbsize = altrbsize; }

	ckb_keyboard *kbd = calloc(1, sizeof(ckb_keyboard));

	is_in_background = 0;
	__sighandler_t old_ttin = signal(SIGTTIN, sighandler);
	__sighandler_t old_ttou = signal(SIGTTOU, sighandler);

	kbd->fd = open(filename, O_RDWR);
	if (kbd->fd < 0 || is_in_background) {
		perror("ckb_init: open failed");
		free(kbd);
		return 0;
	}

	/* set tty to pass through keypresses unprocessed */
	if (tcgetattr(kbd->fd, &kbd->old_termios) < 0 || is_in_background) {
		perror("ckb_init: tcgetattr failed");
		return 0;
	}

	struct termios tio = kbd->old_termios;
	tio.c_lflag &= ~(ICANON | ECHO | ISIG);
	tio.c_iflag &= ~(ISTRIP | IGNCR | ICRNL | INLCR | IXOFF | IXON);
	tio.c_iflag |= IGNBRK;
	tio.c_cc[VMIN] = 0;
	tio.c_cc[VTIME] = 0;

	if (tcsetattr(kbd->fd, TCSANOW, &tio) < 0 || is_in_background) {
		perror("ckb_init: tcsetattr failed");
		free(kbd);
		return 0;
	}

	/* set keyboard to medium raw mode (all keys returned as single byte values) */
	if (ioctl(kbd->fd, KDGKBMODE, &kbd->old_mode) < 0) {
		perror("ckb_init: get mode failed");
		kbd->old_mode = K_XLATE;
	}
	if (ioctl(kbd->fd, KDSKBMODE, K_MEDIUMRAW) < 0 || is_in_background) {
		perror("ckb_init: set raw mode failed");
		tcsetattr(kbd->fd, TCSANOW, &(kbd->old_termios));
		close(kbd->fd);
		free(kbd);
		return 0;
	}

	/* keyboard led status */
	if (ioctl(kbd->fd, KDGKBLED, &kbd->old_kbled) != 0) {
		perror("ckb_init: get keylock status failed");
		kbd->old_kbled = 0x7f;
	} else {
		kbd->modifiers = kbd->old_kbled & LED_CAP ? CKB_MOD_CAPS : 0;
		kbd->modifiers |= kbd->old_kbled & LED_NUM ? CKB_MOD_NUM : 0;
		kbd->modifiers |= kbd->old_kbled & LED_SCR ? CKB_MOD_SCROLL : 0;
	}

	/* accent table */
	if (ioctl(kbd->fd, KDGKBDIACRUC, &kbd->diac_table) != 0) {
		kbd->diac_table.kb_cnt = 0;
	}
	kbd->accent = 0;

	/* keyboard ring buffer */
	kbd->ringbuffer = calloc(rbsize, sizeof(ckb_key));
	if (kbd->ringbuffer && !is_in_background) {
		kbd->rbsize = rbsize;
		kbd->rbfirst = 0;
		kbd->rblast = 0;
	} else {
		perror("ckb_init: ringbuffer allocation failed");
		kbd->ringbuffer = 0;
		ckb_close(kbd);
		return 0;
	}

	signal(SIGTTIN, old_ttin);
	signal(SIGTTOU, old_ttou);

	return kbd;
}

int ckb_poll(ckb_keyboard *kbd)
{
	unsigned char code = 0;
	int readlen, result = 1;

	/* Read keyboard data */
	if ((readlen = read(kbd->fd, &code, 1)) > 0) {
		result = ckb_handle_event(kbd, code);
	}

	if (readlen < 0) {
		/* Error, we try again next time */
		perror("ckb_poll: Error reading keyboard");
		result = 0;
	}

	return result;
}

int ckb_getkey(ckb_keyboard *kbd, ckb_key *key)
{
	if (kbd->rblast == kbd->rbfirst) {
		return 0;
	}
	*key = kbd->ringbuffer[kbd->rbfirst];
	kbd->rbfirst = (kbd->rbfirst + 1) % kbd->rbsize;
	return 1;
}

int ckb_ischar(ckb_key *key)
{
	return key->sym != CKB_NONE &&
			(key->label < CKB_SPECIAL || 
			 key->label == CKB_ENTER ||
			 ckb_isnumpad(key));
}

int ckb_isfnkey(ckb_key *key)
{
	return key->label >= CKB_F1 && key->label <= CKB_F12;
}

int ckb_isnumpad(ckb_key *key)
{
	return key->label > CKB_NUM && key->label <= CKB_N9;
}

int ckb_ismod(ckb_key *key)
{
	return key->label >= CKB_LSHIFT && key->label <= CKB_RCTRL;
}

int ckb_islock(ckb_key *key)
{
	return key->label == CKB_CAPS || key->label == CKB_NUM || key->label == CKB_SCROLL;
}