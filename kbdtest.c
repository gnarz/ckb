/* gcc -g -Wall -Wextra -o kbdtest kbdtest.c ckb_keyboard.c ckb_keyname.c
 *
 * make sure to select a unicode font for your console before running this
 */

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ckb_keyboard.h"
#include "ckb_keyname.h"
#include "mini_utf8.h"

const char* flags(ckb_key *key)
{
	static char f[8];
	f[0] = ckb_ischar(key) ? 'c' : '-';
	f[1] = ckb_isfnkey(key) ? 'f' : '-';
	f[2] = ckb_isnumpad(key) ? 'n' : '-';
	f[3] = ckb_ismod(key) ? 'm' : '-';
	f[4] = ckb_islock(key) ? 'l' : '-';
	f[5] = ckb_iskeyup(key) ? 'u' : '-';
	f[6] = ckb_iskeydown(key) ? 'd' : '-';
	f[7] = 0;
	return f;
}

int mssleep(long ms)
{
	struct timespec req, rem;
	req.tv_sec = ms / 1000;
	req.tv_nsec = (ms % 1000) * 1000000;
	nanosleep(&req, &rem);
	return rem.tv_sec * 1000 + rem.tv_nsec / 1000000;
}

int main(int argc, char **argv)
{
	argc = argc; argv = argv; /* unused */

	setvbuf(stdout, 0, _IONBF, 0);

	ckb_keyboard *kbd = ckb_init(0, 0);

	puts("\n---press escape twice to leave.\n");

	int esc = 0;
	ckb_key key;
	while (ckb_poll(kbd)) {
		if (ckb_getkey(kbd, &key)) {
			printf("key %s %3d label %04x mod %02x raw %04x sym %04x flags %s name %s\n",
				key.which == CKB_E_RELEASE ? "up" : key.which == CKB_E_PRESS ? "dn" : "rp",
				key.code, key.label, key.mod, key.raw, key.sym, flags(&key), ckb_keyname(&key));
			if (key.which == CKB_E_PRESS) {
				if (key.label == CKB_ESCAPE) {
					if (esc) { break; }
					esc = 1;
				} else {
					esc = 0;
				}
			}
		}
		mssleep(10);
	}

	ckb_close(kbd);

	puts("\n---bye.\n");

	return 0;
}
